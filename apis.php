<?php
    
	/* 
		Different Methods for Web API
 	*/
	date_default_timezone_set("America/Denver");
	
	require_once("Rest.inc.php");
	
	class API extends REST {
		
		
	
		public $data = "";
		
		const DB_SERVER = "localhost";
		const DB_USER = "naviun5_mug";
		const DB_PASSWORD = 'eaUC=KbzL~TB';
		const DB = "naviun5_mug";
		
		private $db = NULL;
	
		public function __construct(){
			parent::__construct();				// Init parent constructor
			$this->dbConnect();					// Initiate Database connection
		}
		
		/*
		 *  Database connection 
		*/
		private function dbConnect(){
			$this->db = mysql_connect(self::DB_SERVER,self::DB_USER,self::DB_PASSWORD);
			if($this->db)
				mysql_select_db(self::DB,$this->db);
		}
		
		/*
		 * Public method for access api.
		 * This method dynamically call the method based on the query string
		 *
		 */
		public function processApi(){
			$func = strtolower(trim(str_replace("/","",$_REQUEST['rquest'])));
			if((int)method_exists($this,$func) > 0)
				$this->$func();
			else
				$this->response('',404);
			// If the method not exist with in this class, response would be "Page not found".
		}
		
		/* 
		 *	Simple login API
		 *  Login must be POST method
		 *  email : <USER EMAIL>
		 *  pwd : <USER PASSWORD>
		 */
		 //storing token in database 
		 
		public function registerDevice($email,$token){
			if(!$this->isEmailExist($email)){
				$stmt = $this->db->prepare("INSERT INTO devices (email, token) VALUES (?,?) ");
				$stmt->bind_param("ss",$email,$token);
				if($stmt->execute())
					return 0; //return 0 means success
				return 1; //return 1 means failure
			}else{
				return 2; //returning 2 means email already exist
			}
		}
		
		private function isEmailexist($email){
			$stmt = $this->db->prepare("SELECT id FROM devices WHERE email = ?");
			$stmt->bind_param("s",$email);
			$stmt->execute();
			$stmt->store_result();
			$num_rows = $stmt->num_rows;
			$stmt->close();
			return $num_rows > 0;
		}
		
		//getting all tokens to send push to all devices
		public function getAllTokens(){
			$stmt = $this->db->prepare("SELECT token FROM devices");
			$stmt->execute(); 
			$result = $stmt->get_result();
			$tokens = array(); 
			while($token = $result->fetch_assoc()){
				array_push($tokens, $token['token']);
			}
			return $tokens; 
		}

		//getting a specified token to send push to selected device
		public function getTokenByEmail($email){
			$stmt = $this->db->prepare("SELECT token FROM devices WHERE email = ?");
			$stmt->bind_param("s",$email);
			$stmt->execute(); 
			$result = $stmt->get_result()->fetch_assoc();
			return array($result['token']);        
		}

		//getting all the registered devices from database 
		public function getAllDevices(){
			$stmt = $this->db->prepare("SELECT * FROM devices");
			$stmt->execute();
			$result = $stmt->get_result();
			return $result; 
		}
		// END Token Inserted data
		public function registerDeviceintoDb(){
			if($_SERVER['REQUEST_METHOD']=='POST'){
			$token = $_POST['token'];
			$email = $_POST['email'];
			$result = $this->registerDevice($email,$token);

				if($result == 0){
					$response['error'] = false; 
					$response['message'] = 'Device registered successfully';
				}elseif($result == 2){
					$response['error'] = true; 
					$response['message'] = 'Device already registered';
				}else{
					$response['error'] = true;
					$response['message']='Device not registered';
				}
			}else{
				$response['error']=true;
				$response['message']='Invalid Request...';
			}

		echo json_encode($response);
	}
		public function getRegisterDevicesfromDb(){
			$devices = $this->getAllDevices();
			$response = array(); 

			$response['error'] = false; 
			$response['devices'] = array(); 

			while($device = $devices->fetch_assoc()){
				$temp = array();
				$temp['id']=$device['id'];
				$temp['email']=$device['email'];
				$temp['token']=$device['token'];
				array_push($response['devices'],$temp);
			}

			echo json_encode($response);
		}
		
		//Simple Login function
		
		private function login(){
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			/* if($this->get_request_method() != "POST")
			{
			$this->response('',406);
			} */
			$username = $_REQUEST['username'];
			$password = $_REQUEST['password'];
			if(!empty($password)){				
					
					 $sql = mysql_query("SELECT user.*,tbl_store.store_name FROM user INNER JOIN tbl_store ON user.store = tbl_store.id WHERE user.username='".$username."' && user.password='".md5($password)."'", $this->db);
					if(mysql_num_rows($sql) > 0){
						$result = mysql_fetch_array($sql,MYSQL_ASSOC); 
						if($result['role'] == 'manager'){
							$id = explode(",",$result['store']);
							$result['store'] = $id[0];
						}
						// If success everythig is good send header as "OK" and user details
						$response = array('status' => "success", "msg" => "" ,"data" =>array($result));
						$this->response($this->json($response), 200);
					}else{
						$response = array('status' => "fail", "msg" => "wrong user details" ,"data" =>"");
						$this->response($this->json($response), 200);	// If no records "No Content" status
					}
					
			}else{
			// If invalid inputs "Bad Request" status message and reason
			$error = array('status' => "Failed", "msg" => "Invalid detail");
			$this->response($this->json($error), 400);
			}
		}
		
		//funtion for Dashboard API for employee
	private function dashboard_data_for_employee(){
			/* if($this->get_request_method() != "POST")
				{
				$this->response('',406);
				}	 */
			// Cross validation if the request method is POST else it will return "Not Acceptable" status	
			$uid =$_REQUEST['uid'];
			$month = date('F');
			$year = date('Y'); 
			// Input validations
			if(!empty($uid)){
					//$sql = mysql_query("SELECT * FROM tbl_goal WHERE uid='".$uid."' AND month='".$month."'", $this->db);
					$sql = mysql_query("SELECT tbl_goal.*,data.hairs_total,data.beard_total,data.product_total FROM tbl_goal INNER JOIN data ON tbl_goal.uid = data.uid WHERE tbl_goal.uid='".$uid."' AND tbl_goal.month='".$month."' AND tbl_goal.month='".$month."' AND tbl_goal.year='".$year."'", $this->db);
					if(mysql_num_rows($sql) > 0){
						$result = mysql_fetch_array($sql,MYSQL_ASSOC);
						
						$sqls = mysql_query("SELECT * FROM tbl_whatsnew where status=1", $this->db);
						$results ="";
						while($rlt = mysql_fetch_array($sqls,MYSQL_ASSOC)){
							$results[] = $rlt;
						}
						// If success everythig is good send header as "OK" and user details
						$msg =  array('status' => "success", "msg" => "Current Goal of Employee", 'data' => array('user_Stat'=>$result,'whats_new'=>$results));
						//$this->response( $this->json($result), 200 );
						$this->response($this->json($msg), 200 );
					$this->response('', 204);	// If no records "No Content" status	
				}
			// If invalid inputs "Bad Request" status message and reason
			$error = array('status' => "Failed", "msg" => "No Record Found");
			$this->response($this->json($error), 404);
		}
	}
		private function get_stats(){
			/* if($this->get_request_method() != "POST")
				{
				$this->response('',406);
				}	 */
			// Cross validation if the request method is POST else it will return "Not Acceptable" status	
			$uid =  $_REQUEST['uid'];
			$month = date('F');
			$year = date('Y'); 
			// Input validations
			if(!empty($uid)){
					$sql = mysql_query("SELECT *, sum(net) as net FROM data WHERE uid='".$uid."' AND month='".$month."' AND year='".$year."'", $this->db);
					if(mysql_num_rows($sql) > 0){
						$result = mysql_fetch_array($sql,MYSQL_ASSOC);
						
						// If success everythig is good send header as "OK" and user details
						$msg =  array('status' => "success", "msg" => "stat data from csv file", 'data' => array($result));
						//$this->response( $this->json($result), 200 );
						$this->response($this->json($msg), 200 );
				
					$this->response('', 204);	// If no records "No Content" status
					
			}
			// If invalid inputs "Bad Request" status message and reason
			$error = array('status' => "Failed", "msg" => "No Record Found");
			$this->response($this->json($error), 404);
		}
	}
	//API for get task list
	private function get_task_list(){
			/* if($this->get_request_method() != "POST")
				{
				$this->response('',406);
				} */	
			// Cross validation if the request method is POST else it will return "Not Acceptable" status	
			$uid =$_REQUEST['uid'];
			// Input validations
			if(!empty($uid)){
					$sql = mysql_query("SELECT * FROM tbl_task_list WHERE uid='".$uid."' AND  DATE(date)=CURDATE()", $this->db);
					if(mysql_num_rows($sql) > 0){
						$result = array();
						while($rlt = mysql_fetch_array($sql,MYSQL_ASSOC)){
							$result[] = $rlt;
						}
						
						// If success everythig is good send header as "OK" and user details
						$msg =  array('status' => "success", "msg" => "Task list for today", 'data' => $result);
						//$this->response( $this->json($result), 200 );
						$this->response($this->json($msg), 200 );
				
					$this->response('', 204);	// If no records "No Content" status
					
			}
			// If invalid inputs "Bad Request" status message and reason
			$error = array('status' => "Failed", "msg" => "No Record Found");
			$this->response($this->json($error), 404);
		}
	}
	//API for getr task list
	private function add_new_task_into_list(){
			/* if($this->get_request_method() != "POST")
				{
				$this->response('',406);
				} */	
			$uid =$_REQUEST['uid'];
			$task =$_REQUEST['task'];
			$date = date('Y-m-d H:i:s');
			// Input validations
			if(!empty($uid)){
					$sql = mysql_query("INSERT into tbl_task_list(uid,tasks,status,date) VALUES('".$uid."','".$task."','0','".$date."')");
					if($sql == true){
						$sqls = mysql_query("SELECT * FROM tbl_task_list WHERE uid='".$uid."' AND DATE(date)=CURDATE()", $this->db);
							$result = array();
							while($rlt = mysql_fetch_array($sqls,MYSQL_ASSOC)){
								$result[] = $rlt;
							}
						
						$msg =  array('status' => "success", "msg" => "Task Added for today", 'data' =>$result);
						$this->response($this->json($msg), 200 );
					}
					$this->response('', 204);	// If no records "No Content" status
					
			}
			// If invalid inputs "Bad Request" status message and reason
			$error = array('status' => "Failed", "msg" => "No Record Found");
			$this->response($this->json($error), 404);
	}
	//funtion for fetch goal set by employee
	private function get_current_month_goal(){
			/* if($this->get_request_method() != "POST")
				{
				$this->response('',406);
				} */	
			// Cross validation if the request method is POST else it will return "Not Acceptable" status	
			$uid =$_REQUEST['uid'];
			$month = date('F');
			$year = date('Y'); 
			// Input validations
			if(!empty($uid)){
					//$sql = mysql_query("SELECT * FROM tbl_goal WHERE uid='".$uid."' AND month='".$month."'", $this->db);
					$sql = mysql_query("SELECT tbl_goal.*,sum(data.hairs_total) as hairs_total,sum(data.beard_total) as beard_total,sum(data.product_total) as  product_total FROM tbl_goal INNER JOIN data ON tbl_goal.uid = data.uid WHERE tbl_goal.uid='".$uid."' AND data.month='".$month."' AND tbl_goal.month='".$month."' AND data.year='".$year."'", $this->db);
					if(mysql_num_rows($sql) > 0){
						$result = mysql_fetch_array($sql,MYSQL_ASSOC);
						$sqlss = mysql_query("SELECT * FROM tbl_helpfultips where status=1", $this->db);
						$resultss ="";
						while($rlts = mysql_fetch_array($sqlss,MYSQL_ASSOC)){
							$resultss[] = $rlts;
						}
						// If success everythig is good send header as "OK" and user details
						$msg =  array('status' => "success", "msg" => "Current Goal of Employee", 'data' => array('stat'=> $result,'helpful_tips'=>$resultss));
						//$this->response( $this->json($result), 200 );
						$this->response($this->json($msg), 200 );
				
					$this->response('', 204);	// If no records "No Content" status
					
			}
			// If invalid inputs "Bad Request" status message and reason
			$error = array('status' => "Failed", "msg" => "No Record Found");
			$this->response($this->json($error), 404);
		}
	}
	
	
	//funtion for fetch goal set by employee
	private function get_next_previous_month_goal_and_data(){
			/* if($this->get_request_method() != "POST")
				{
				$this->response('',406);
				} */
			// Cross validation if the request method is POST else it will return "Not Acceptable" status	
			$uid =$_REQUEST['uid'];
			$month = $_REQUEST['month'];
			$year = $_REQUEST['year']; 
			// Input validations
			if(!empty($uid)){
					//$sql = mysql_query("SELECT * FROM tbl_goal WHERE uid='".$uid."' AND month='".$month."'", $this->db);
					$sql = mysql_query("SELECT tbl_goal.*,sum(data.hairs_total) as hairs_total,sum(data.beard_total) as beard_total,sum(data.product_total) as  product_total FROM tbl_goal INNER JOIN data ON tbl_goal.uid = data.uid WHERE tbl_goal.uid='".$uid."' AND data.month='".$month."' AND tbl_goal.month='".$month."' AND data.year='".$year."'", $this->db);
					if(mysql_num_rows($sql) > 0){
						$result = mysql_fetch_array($sql,MYSQL_ASSOC);
						$sqlss = mysql_query("SELECT * FROM tbl_helpfultips where status=1", $this->db);
						$resultss ="";
						while($rlts = mysql_fetch_array($sqlss,MYSQL_ASSOC)){
							$resultss[] = $rlts;
						}
						if(empty($result['id'])){
							$msg =  array('status' => "fail", "msg" => "No result Found", 'data' => array('stat'=> 'No result Found','helpful_tips'=>$resultss));
							$this->response($this->json($msg), 200 );
						}else{ 
						// If success everythig is good send header as "OK" and user details
							$msg =  array('status' => "success", "msg" => "Next-Previous data and goal of an Employee", 'data' => array('stat'=> $result,'helpful_tips'=>$resultss));
							$this->response($this->json($msg), 200 );
						}
				
					$this->response('', 204);	// If no records "No Content" status
					
			}
			// If invalid inputs "Bad Request" status message and reason
			$error = array('status' => "Failed", "msg" => "No Record Found");
			$this->response($this->json($error), 404);
		}
	}
	
	
	//function for SET goal set by employee
	private function set_current_month_goal(){
			/* if($this->get_request_method() != "POST")
				{
				$this->response('',406);
				}	 */
			// Cross validation if the request method is POST else it will return "Not Acceptable" status	
			$uid =$_REQUEST['uid'];
			$new_goal =$_REQUEST['new_goal'];
			$month = date('F');
			$year = date('Y');
			$date = date('Y-m-d H:i:s');
			// Input validations
			if(!empty($uid)){
					$sql = mysql_query("SELECT * FROM tbl_goal WHERE uid='".$uid."' AND month='".$month."'", $this->db);
					if(mysql_num_rows($sql) > 0){

						$mydata = mysql_query("UPDATE tbl_goal SET monthly_target = '".$new_goal."' WHERE uid = '".$uid."' AND month = '".$month."'");
						// If success everything is good send header as "OK" and user details
						$msg =  array('status' => "success", "msg" => "New Goal of Employee set", 'data' => 'Goal Updated');
						$this->response($this->json($msg), 200 );
					}else{
						$mydata = mysql_query("INSERT INTO tbl_goal (uid,month,year,monthly_target,status,date) VALUES('".$uid."','".$month."','".$year."','".$new_goal."','1','".$date."')");
						// If success everythig is good send header as "OK" and user details
						$msg =  array('status' => "success", "msg" => "New Goal of Employee set", 'data' => 'Goal Added');
						//$this->response( $this->json($result), 200 );
						$this->response($this->json($msg), 200 );
					}
			// If invalid inputs "Bad Request" status message and reason
			$error = array('status' => "Failed", "msg" => "No Record Found");
			$this->response($this->json($error), 404);
		}
	}
	
	//API for get appointment list for today on basis of store ID
	
	private function get_appointment_list(){
			/* if($this->get_request_method() != "POST")
				{
				$this->response('',406);
				} */
			// Cross validation if the request method is POST else it will return "Not Acceptable" status	
			$storeid =$_REQUEST['store_id'];
			//Input validations
			if(!empty($storeid)){
					$sql = mysql_query("SELECT * FROM tbl_appointment WHERE store='".$storeid."' AND  DATE(date)=CURDATE() order by time asc", $this->db);
					if(mysql_num_rows($sql) > 0){
						$result = array();
						while($rlt = mysql_fetch_array($sql,MYSQL_ASSOC)){
							$result[] = $rlt;
						}
						
						// If success everythig is good send header as "OK" and user details
						$msg =  array('status' => "success", "msg" => "Appointment list for today", 'data' => $result);
						//$this->response( $this->json($result), 200 );
						$this->response($this->json($msg), 200 );
				
					$this->response('', 204);	// If no records "No Content" status
					
			}
			// If invalid inputs "Bad Request" status message and reason
			$error = array('status' => "Failed", "msg" => "No Record Found");
			$this->response($this->json($error), 404);
		}
	}
	
		//API for get shifts list for this month basis of user ID
	
	private function get_shift_list(){
			/* if($this->get_request_method() != "POST")
				{
				$this->response('',406);
				} */
			$uid =$_REQUEST['uid'];
			//Input validations
			if(!empty($uid)){
					$sql = mysql_query("SELECT * FROM tbl_shift WHERE uid='".$uid."' and YEAR(date) = YEAR(CURRENT_DATE()) AND MONTH(date) = MONTH(CURRENT_DATE()) order by date ASC", $this->db);
					if(mysql_num_rows($sql) > 0){
						$result = array();
						while($rlt = mysql_fetch_array($sql,MYSQL_ASSOC)){
							$result[] = $rlt;
						}
						
						// If success everythig is good send header as "OK" and user details
						$msg =  array('status' => "success", "msg" => "Shift list for This Month of Current User", 'data' => $result);
						//$this->response( $this->json($result), 200 );
						$this->response($this->json($msg), 200 );
				
					$this->response('', 204);	// If no records "No Content" status
					
			}
			// If invalid inputs "Bad Request" status message and reason
			$error = array('status' => "Failed", "msg" => "No Record Found");
			$this->response($this->json($error), 404);
		}
	}
	//API for insert employee chat and send message list
	private function store_employee_message_into_group_chat(){
			/* if($this->get_request_method() != "POST")
			{
			$this->response('',406);
			} */
			$uid =$_REQUEST['uid'];
			$message =$_REQUEST['message'];
			$store_id =$_REQUEST['store_id'];
			$date = date('Y-m-d H:i:s');
			// Input validations
			if(!empty($uid)){
					$sql = mysql_query("INSERT into tbl_employee_chat(uid,message,status,date) VALUES('".$uid."','".$message."','1','".$date."')");
					if($sql == true){
						$msg =  array('status' => "success", "msg" => "Message Successfully Sent", 'data' =>'');
						$this->response($this->json($msg), 200 );
					}
					$this->response('', 204);	// If no records "No Content" status
					
			}
			// If invalid inputs "Bad Request" status message and reason
			$error = array('status' => "Failed", "msg" => "No msg Found");
			$this->response($this->json($error), 404);
	}
	
	
	//API for Listing employee chat
	private function listing_of_messages_in_group_chat(){
			/* if($this->get_request_method() != "POST")
				{
				$this->response('',406);
				} */	
			$store_id =$_REQUEST['store_id'];
			// Input validations
			if(!empty($store_id)){
					
				$sqls = mysql_query("SELECT user.name,user.username,user.profile_img, tbl_employee_chat.* FROM user INNER JOIN tbl_employee_chat ON user.id = tbl_employee_chat.uid where user.store='".$store_id."' and YEAR(tbl_employee_chat.date) = YEAR(CURRENT_DATE()) AND MONTH(tbl_employee_chat.date) = MONTH(CURRENT_DATE()) order by id asc", $this->db);
					$result = array();
					while($rlt = mysql_fetch_array($sqls,MYSQL_ASSOC)){
						$result[] = $rlt;
					}
						
					$msg =  array('status' => "success", "msg" => "message added into group chat", 'data' =>$result);
					$this->response($this->json($msg), 200 );
					
			}
			// If invalid inputs "Bad Request" status message and reason
			$error = array('status' => "Failed", "msg" => "No Record Found");
			$this->response($this->json($error), 404);
	}
	
	// This API is used for USER LISTING
	private function store_employees_listing(){
			/* if($this->get_request_method() != "POST")
				{
				$this->response('',406);
				} */			
			$store_id =$_REQUEST['store_id'];
			// Input validations
			if(!empty($store_id)){
					
				$sqls = mysql_query("SELECT * FROM user where store='".$store_id."' and role='employee'", $this->db);
					$result = array();
					while($rlt = mysql_fetch_array($sqls,MYSQL_ASSOC)){
						$result[] = $rlt;
					}
						
					$msg =  array('status' => "success", "msg" => "User Listing based on Stores", 'data' =>$result);
					$this->response($this->json($msg), 200 );
					
			}
			// If invalid inputs "Bad Request" status message and reason
			$error = array('status' => "Failed", "msg" => "No Record Found");
			$this->response($this->json($error), 404);
	}
	
	//function for update tasks to done by employee
	private function update_tasklist_when_done(){
			/* if($this->get_request_method() != "POST")
			{
			$this->response('',406);
			}	 */
			$uid =$_REQUEST['uid'];
			$task_id =$_REQUEST['task_id'];
			// Input validations
			if(!empty($uid)){
				$mydata = mysql_query("UPDATE tbl_task_list SET status = '1' WHERE uid = '".$uid."' AND id = '".$task_id."'");
					// If success everything is good send header as "OK" and user details
					$msg =  array('status' => "success", "msg" => "Task Marked as done.", 'data' => 'task Updated');
					//$this->response( $this->json($result), 200 );
					$this->response($this->json($msg), 200 );
				}
			// If invalid inputs "Bad Request" status message and reason
			$error = array('status' => "Failed", "msg" => "No Record Found");
			$this->response($this->json($error), 404);
	}
	
	//function for Manager screen data
	private function manager_screen_data_for_above(){
		
			/* if($this->get_request_method() != "POST")
				{
				$this->response('',406);
				} */
			$store_id = $_REQUEST['store_id'];
			$month = date('F');
			$year = date('Y');
			// Input validations
			if(!empty($store_id)){
				$mydata = mysql_query("SELECT sum(monthly_target) as month_target, sum(net) as achieved, sum(hairs_total) as hairs_total, sum(beard_total) as beard_total, sum(product_total) as product_total FROM user INNER JOIN tbl_goal ON user.id = tbl_goal.uid INNER JOIN data ON user.id = data.uid where user.store='".$store_id."' and data.month='".$month."' and data.year='".$year."' and YEAR(tbl_goal.date) = YEAR(CURRENT_DATE()) AND MONTH(tbl_goal.date) = MONTH(CURRENT_DATE())");
				
					$result = array();
					while($rlt = mysql_fetch_array($mydata,MYSQL_ASSOC)){
						$result[] = $rlt;
					}
					
				$userdata = mysql_query("SELECT user.id, user.username, tbl_goal.monthly_target,data.net as latest_earning FROM user INNER JOIN tbl_goal ON user.id = tbl_goal.uid INNER JOIN data ON user.id = data.uid where user.store='".$store_id."' and data.month='".$month."' and data.year='".$year."' and YEAR(tbl_goal.date) = YEAR(CURRENT_DATE()) AND MONTH(tbl_goal.date) = MONTH(CURRENT_DATE())  group by user.id");
				$results = array();
				while($rlts = mysql_fetch_array($userdata,MYSQL_ASSOC)){
					$results[] = $rlts;
				}
				if(!empty($results)){
					$msg =  array('status' => "success", "msg" => "Manager Stats with user data", 'data' => array('manager_stat' => $result,'user_data' => $results));
					$this->response($this->json($msg), 200 );
					}else{
						$msg =  array('status' => "failure", "msg" => "No Record Found", 'data' => "");
						$this->response($this->json($msg), 200 );
					}
				}
			// If invalid inputs "Bad Request" status message and reason
			$error = array('status' => "Failed", "msg" => "No Record Found");
			$this->response($this->json($error), 404);
	}
	//function for Manager screen data for next previous 
	private function manager_screen_data_for_next_previous(){
		
			/* if($this->get_request_method() != "POST")
				{
				$this->response('',406);
				} */	
			$store_id = $_REQUEST['store_id'];
			$month = $_REQUEST['month'];
			$year = $_REQUEST['year'];
			// Input validations
			if(!empty($store_id)){
				
				$achieve = mysql_query("SELECT u.store, COALESCE(MAX(t1.achieved), 0) AS achieved, COALESCE(MAX(t1.hairs_total), 0) AS hairs_total, COALESCE(MAX(t1.beard_total), 0) AS beard_total, COALESCE(MAX(t1.product_total), 0) AS product_total, COALESCE(MAX(t2.month_target), 0) AS month_target FROM user u LEFT JOIN ( SELECT usr.store, SUM(d.net) AS achieved, SUM(d.hairs_total) AS hairs_total, SUM(d.beard_total) AS beard_total, SUM(d.product_total) AS product_total FROM data d INNER JOIN user usr ON d.uid = usr.id WHERE d.month = '".$month."' and d.year = '".$year."' GROUP BY usr.store ) t1 ON u.store = t1.store LEFT JOIN ( SELECT usr.store, SUM(t.monthly_target) AS month_target FROM tbl_goal t INNER JOIN user usr ON t.uid = usr.id WHERE t.month = '".$month."' and t.year = '".$year."' GROUP BY usr.store ) t2 ON u.store = t2.store WHERE u.store = '".$store_id."'");
				$result = array();
				while($rlts = mysql_fetch_array($achieve,MYSQL_ASSOC)){
						$result[] = $rlts;
				}
				$userdata = mysql_query("SELECT user.id, user.username, tbl_goal.monthly_target, data.net FROM user INNER JOIN tbl_goal ON user.id = tbl_goal.uid INNER JOIN data ON user.id = data.uid where user.store='".$store_id."' and data.month='".$month."' and tbl_goal.month='".$month."' and data.year='".$year."' group by user.username");
				//$userdata = mysql_query("SELECT u.store,u.id, u.username, COALESCE(MAX(t1.net), 0) AS net, COALESCE(MAX(t2.monthly_target), 0) AS monthly_target FROM user u LEFT JOIN ( SELECT usr.store, SUM(d.net) AS net FROM data d INNER JOIN user usr ON d.uid = usr.id WHERE d.month = '".$month."' and d.year = '".$year."' GROUP BY usr.store ) t1 ON u.store = t1.store LEFT JOIN ( SELECT usr.store, SUM(t.monthly_target) AS monthly_target FROM tbl_goal t INNER JOIN user usr ON t.uid = usr.id WHERE t.month = '".$month."' and t.year = '".$year."'GROUP BY usr.store ) t2 ON u.store = t2.store WHERE u.store = '".$store_id."'");
				$results = array();
				while($rlts = mysql_fetch_array($userdata,MYSQL_ASSOC)){
					$results[] = $rlts;
				}
				if(!empty($results[0]['username'])){
					$msg =  array('status' => "success", "msg" => "Manager Stats with user data", 'data' => array('manager_stat' => $result,'user_data' => $results));
					$this->response($this->json($msg), 200 );
					}else{
						$msg =  array('status' => "failure", "msg" => "No Record Found", 'data' => "");
						$this->response($this->json($msg), 200 );
					}
				}
			// If invalid inputs "Bad Request" status message and reason
			$error = array('status' => "Failed", "msg" => "No Record Found");
			$this->response($this->json($error), 404);
	}
	
	//Function for User Data detail from Manager Stats
	private function user_details_from_manager_stat(){
			/* if($this->get_request_method() != "POST")
				{
				$this->response('',406);
				} */
			// Cross validation if the request method is POST else it will return "Not Acceptable" status	
			$user_id =$_REQUEST['user_id'];
			// Input validations
			if(!empty($user_id)){
					$sql = mysql_query("SELECT user.name, tbl_goal.monthly_target, hairs_total,data.beard_total,data.product_total,data.net as total FROM user INNER JOIN data ON user.id = data.uid INNER JOIN tbl_goal ON user.id = tbl_goal.uid WHERE data.uid='".$user_id."' and YEAR(tbl_goal.date) = YEAR(CURRENT_DATE()) AND MONTH(tbl_goal.date) = MONTH(CURRENT_DATE())", $this->db);
					if(mysql_num_rows($sql) > 0){
						$result = mysql_fetch_array($sql,MYSQL_ASSOC);
						
						// If success everythig is good send header as "OK" and user details
						$msg =  array('status' => "success", "msg" => "User Data detail from Manager Stats", 'data' => array($result));
						//$this->response( $this->json($result), 200 );
						$this->response($this->json($msg), 200 );
				
					$this->response('', 204);	// If no records "No Content" status
					
			}
			// If invalid inputs "Bad Request" status message and reason
			$error = array('status' => "Failed", "msg" => "No Record Found");
			$this->response($this->json($error), 404);
		}
	}
	
	/* API's For Manager Screens. */
	
	// This API Store List
	private function store_listing_for_manager_screen(){
		/* if($this->get_request_method() != "POST")
				{
				$this->response('',406);
				}	 */
		$uid = $_REQUEST['uid'];
		$sqls = mysql_query("SELECT store FROM user where role='manager' AND id=".$uid, $this->db);
		$result = array();
		while($rlt = mysql_fetch_array($sqls,MYSQL_ASSOC)){
			$result[] = $rlt['store'];
		}		
		$id = explode(",",$result[0]);
		$results = array();
		if(!empty($result)){
			foreach($id as $store_id){
				$sql = mysql_query("SELECT * FROM tbl_store where id=".$store_id, $this->db);
				while($rlts = mysql_fetch_array($sql,MYSQL_ASSOC)){
					$results[] = $rlts;
				}	
			}
			$msg =  array('status' => "success", "msg" => "Store Listing For Manager", 'data' =>$results);
			$this->response($this->json($msg), 200 );
		}else{
			$msg =  array('status' => "success", "msg" => "Store Listing For Manager", 'data' =>'Invalid User - User have not Manager Role');
			$this->response($this->json($msg), 200 );
		}
	// If invalid inputs "Bad Request" status message and reason
		$error = array('status' => "Failed", "msg" => "No Record Found");
		$this->response($this->json($error), 404);
	}
	//API for get task list
	private function get_task_list_for_manager(){
			/* if($this->get_request_method() != "POST")
				{
				$this->response('',406);
				} */
			$store_id =$_REQUEST['store_id'];
			$sqls = mysql_query("SELECT id FROM user where role='employee' AND store=".$store_id, $this->db);
			$result = array();
			while($rlt = mysql_fetch_array($sqls,MYSQL_ASSOC)){
				$result[] = $rlt;
			}
			/* echo "<pre>";
			print_r($result);
			die; */
			$results = array();
			foreach($result as $user_id){
				//echo $user_id['id'];
				$sql = mysql_query("SELECT user.username, tbl_task_list.tasks, tbl_task_list.status, tbl_task_list.date FROM tbl_task_list inner join user ON tbl_task_list.uid = user.id WHERE tbl_task_list.uid='".$user_id['id']."' AND  DATE(tbl_task_list.date)=CURDATE()", $this->db);
					while($rlts = mysql_fetch_array($sql,MYSQL_ASSOC)){
						$results[] = $rlts;
					}
				}
				if(!empty($results)){
					//If success everything is good send header as "OK" and user details
					$msg =  array('status' => "success", "msg" => "Task list for today Based on Store ID", 'data' => $results);
					$this->response($this->json($msg), 200 );
				}else{
					$msg =  array('status' => "failure", "msg" => "No Record Found", 'data' => '');
					$this->response($this->json($msg), 200 );
				}
	}
	
	// This API is used for Video LISTING
	private function training_videos_listing(){
		/* if($this->get_request_method() != "POST")
				{
				$this->response('',406);
				} */
		$sqls = mysql_query("SELECT * FROM tbl_videos where category='services' and status='1'", $this->db);
		$sql2 = mysql_query("SELECT * FROM tbl_videos where category='products' and status='1'", $this->db);
		$result = array();
		while($rlt = mysql_fetch_array($sqls,MYSQL_ASSOC)){
			$result[] = $rlt;
		}
		$result2 = array();
		while($rlt2 = mysql_fetch_array($sql2,MYSQL_ASSOC)){
			$result2[] = $rlt2;
		}
						
		$msg =  array('status' => "success", "msg" => "Trainign Video Listing For Employees", 'data' => array('HowToVideos' => $result,'ProductsVideos' => $result2));
		$this->response($this->json($msg), 200 );
		// If invalid inputs "Bad Request" status message and reason
		$error = array('status' => "Failed", "msg" => "No Record Found");
		$this->response($this->json($error), 404);
	}
	
	/*
	 *	Encode array into JSON
	*/
	private function json($data){
		if(is_array($data)){
			return json_encode($data);
		}
	}
	
} //End Class API
	
	// Initiiate Library
	
	$api = new API;
	$api->processApi();
?>